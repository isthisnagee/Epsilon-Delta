-------------
![epsilon delta](/static/css/images/ed.png)
-------------

# Epsilon-Delta


<strike>Website coming soon!!</strike> [Website is up](http://isthisnagee.pythonanywhere.com/)

Allows you to do basic epsilon delta proofs.
For a description on notation and what epsilon delta proofs, I recommend reading chapters 1 and 2 of these [notes](http://www.math.toronto.edu/tholden/LectureNotes137_Preview.pdf).

For now, you can do any n\*x &plusmn; c or n\*x\*\*2 &plusmn; c, where n and c are natural numbers.
Coming soon are proofs with all real numbers and trig functions! yay!


Enjoy!

Webiste is no longer up
